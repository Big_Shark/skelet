<?php

use DI\Container;

class Kernel
{
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
        $containerHandler = $this->getContainerHandler();
        $containerHandler->register($this->container);
    }

    protected function getContainerHandler()
    {
        return new ContainerHandler();
    }

    protected function getCommandHandler()
    {
        return $this->container->get(CommandHandler::class);
    }

    protected function getRouteDispatcher()
    {
        return FastRoute\simpleDispatcher([$this->container->get(RouteHandler::class), 'register']);
    }

    /**
     * Dispatches against the provided HTTP method verb and URI.
     *
     * Returns array with one of the following formats:
     *
     *     [self::NOT_FOUND]
     *     [self::METHOD_NOT_ALLOWED, ['GET', 'OTHER_ALLOWED_METHODS']]
     *     [self::FOUND, $handler, ['varName' => 'value', ...]]
     *
     * @param string $httpMethod
     * @param string $uri
     *
     * @return array
     */
    protected function dispatch(\Psr\Http\Message\RequestInterface $request)
    {
        return $this->getRouteDispatcher()->dispatch($request->getMethod(), $request->getUri()->getPath());
    }

    /**
     * @param \Psr\Http\Message\RequestInterface $request
     * @param $handler
     * @param $vars
     * @return \Psr\Http\Message\ResponseInterface
     */
    protected function callHandler($request, $handler, $vars)
    {
        $response = new \Zend\Diactoros\Response();
        return $this->container->call($handler, [
                'request' => $request,
                'response' => $response,
            ] + $vars);
    }

    public function runCli()
    {
        $application = new \Symfony\Component\Console\Application('Skeleton');
        $this->getCommandHandler()->register($application);
        $application->run();
    }

    public function runHttp($request)
    {
        $routeInfo = $this->dispatch($request);

        // TODO: Refactoring this
        if($routeInfo[0] !== FastRoute\Dispatcher::FOUND)
        {
            $messages = [
                404 => 'Not found',
                405 => 'Method Not Allowed'
            ];
            $code = FastRoute\Dispatcher::NOT_FOUND ? 404 : 405;
            $message = $messages[$code];

            throw new \Exception($message, $code);
        }

        $handler = $routeInfo[1];
        $vars = $routeInfo[2];

        $response = $this->callHandler($request, $handler, $vars);

        $this->respond($response);
    }

    /**
     * Send the response the client
     *
     * @param ResponseInterface $response
     */
    public function respond(\Psr\Http\Message\ResponseInterface $response)
    {
        // Send response
        if (!headers_sent()) {
            // Status
            header(sprintf(
                'HTTP/%s %s %s',
                $response->getProtocolVersion(),
                $response->getStatusCode(),
                $response->getReasonPhrase()
            ));

            // Headers
            foreach ($response->getHeaders() as $name => $values) {
                foreach ($values as $value) {
                    header(sprintf('%s: %s', $name, $value), false);
                }
            }
        }

        // Body
        if (!$this->isEmptyResponse($response)) {
            $body = $response->getBody();
            if ($body->isSeekable()) {
                $body->rewind();
            }
            $settings       = $this->container->get('settings');
            $chunkSize      = $settings['responseChunkSize'];

            $contentLength  = $response->getHeaderLine('Content-Length');
            if (!$contentLength) {
                $contentLength = $body->getSize();
            }


            if (isset($contentLength)) {
                $amountToRead = $contentLength;
                while ($amountToRead > 0 && !$body->eof()) {
                    $data = $body->read(min($chunkSize, $amountToRead));
                    echo $data;

                    $amountToRead -= strlen($data);

                    if (connection_status() != CONNECTION_NORMAL) {
                        break;
                    }
                }
            } else {
                while (!$body->eof()) {
                    echo $body->read($chunkSize);
                    if (connection_status() != CONNECTION_NORMAL) {
                        break;
                    }
                }
            }
        }
    }
}