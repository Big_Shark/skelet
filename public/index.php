<?php

include __DIR__.'/../vendor/autoload.php';

$runner = new League\BooBoo\BooBoo([new League\BooBoo\Formatter\HtmlTableFormatter]);
$runner->silenceAllErrors(false);
$runner->register();

$builder = new DI\ContainerBuilder();
$container = $builder->build();

$kernel = new Kernel($container);

$request = Zend\Diactoros\ServerRequestFactory::fromGlobals();

$kernel->runHttp($request);
